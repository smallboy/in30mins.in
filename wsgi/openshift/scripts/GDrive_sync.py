import requests
import httplib2
import pprint
import sys
import csv
import re
import os

from apiclient.discovery import build
from oauth2client.client import SignedJwtAssertionCredentials

from django.core.files import File
from django.contrib.auth.models import User
from django.db import IntegrityError, DataError
from django.core.files.temp import NamedTemporaryFile


_split = re.compile(r'[\0%s]' % re.escape(''.join(
    [os.path.sep, os.path.altsep or ''])))

def secure_filename(path):
    return _split.sub('', path)


# Email of the Service Account.
SERVICE_ACCOUNT_EMAIL = '179763434918-p36gdad26p7ojtkclhuu478dpfrvsdbt@developer.gserviceaccount.com'

# Path to the Service Account's Private Key file.
SERVICE_ACCOUNT_PKCS12_FILE_PATH = 'Almostmarried - Data Sync-bdea70c7fcc4.p12'


def createDriveService():
  """Builds and returns a Drive service object authorized with the given service account.

  Returns:
	Drive service object.
  """
  f = file(SERVICE_ACCOUNT_PKCS12_FILE_PATH, 'rb')
  key = f.read()
  f.close()

  credentials = SignedJwtAssertionCredentials(SERVICE_ACCOUNT_EMAIL, key,
	  scope='https://www.googleapis.com/auth/drive')
  http = httplib2.Http()
  http = credentials.authorize(http)

  return build('drive', 'v2', http=http)



# response = requests.get("https://www.googleapis.com/drive/v2/files/0BwFdS1zYKNhBRlNWMTVLMk5TbmM/children")
# print response

from apiclient import errors

def print_file(service, file_id):
  """Print a file's metadata.

  Args:
	service: Drive API service instance.
	file_id: ID of the file to print metadata for.
  """
  try:
	file = service.files().get(fileId=file_id).execute()

	print 'Title: %s' % file['title']
	print 'MIME type: %s' % file['mimeType']
  except errors.HttpError, error:
	print 'An error occurred: %s' % error


def download_file(service, drive_file):
  """Download a file's content.

  Args:
	service: Drive API service instance.
	drive_file: Drive File instance.

  Returns:
	File's content if successful, None otherwise.
  """
  download_url = drive_file.get('downloadUrl')
  if download_url:
	resp, content = service._http.request(download_url)
	image_filename = resp['content-disposition'].split(";")[1].strip("filename=")

	if resp.status == 200:
	  # print 'Status: %s' % resp
	  return content, image_filename
	else:
	  print 'An error occurred: %s' % resp
	  return None
  else:
	# The file doesn't have any content stored on Drive.
	return None

def print_files_in_folder(service, folder_id):
  """Print files belonging to a folder.

  Args:
	service: Drive API service instance.
	folder_id: ID of the folder to print files from.
  """
  page_token = None
  while True:
	try:
	  param = {}
	  if page_token:
		param['pageToken'] = page_token

	  # # folderId = service.parent.get(folderId=folder_id, parent=parentId)
	  # print folderId
	  # children = service.children().list(folderId=folder_id, **param).execute()
	  # children = service.files().list(q="'{0}' in parents".format(folder_id)).execute()
	  # file = service.files().get(fileId=folder_id).execute()
	  # children = service.files().list(q="title = 'File Name'".format(file['title'])).execute()
	  children	 = service.children().list(folderId=folder_id).execute()

	  print children

	  # import requests
	  # response = requests.get("https://www.googleapis.com/drive/v2/files/%s/children?key=%s".format(folder_id, "AIzaSyBKoeKJoW0aTYItR7e7mSRQMyGI8uwZkqo"))
	  # print response


	  for child in children.get('items', []):
		print 'File Id: %s' % child['id']
	  page_token = children.get('nextPageToken')
	  if not page_token:
		break
	except errors.HttpError, error:
	  print 'An error occurred: %s' % error
	  break

DriveService = createDriveService()
# print_file(DriveService, '0BwFdS1zYKNhBTG9Vbnp4SW44TlE')
# print_files_in_folder(DriveService, '0BwFdS1zYKNhBTG9Vbnp4SW44TlE')


with open('data/bplanners.csv','rb') as csvfile:
	reader = csv.reader(csvfile, delimiter='|')

	slno = 0
	name = 1
	address = 2
	city = 3
	pincode = 4
	google_map = 5
	website = 6
	email = 7
	phone = 8
	alt_phone = 9
	budget = 10
	image = 11
	citation = 12
	facebook = 13
	user_rating = 14

	from core.models import Actor, ActorType, ActorMeta, Location, City

	for row in reader:


		phone_number = re.sub(r"\D+",'', row[phone]) if re.sub(r"\D+",'', row[phone]) else None
		if not phone:
			continue

		actor_type = ActorType.objects.get(name="wear")

		city_name = row[city].strip().lower() if row[city].strip() else None
		city_obj = None
		if city_name:
			city_obj, ccreated = City.objects.get_or_create(name=city_name)
		else:
			continue

		try:
			location, lcreated = Location.objects.get_or_create(
					city=city_obj,
					pincode=row[pincode].strip(),
					address=row[address],
					google_map_link = row[google_map]
				)
		except DataError:
			location, lcreated = Location.objects.get_or_create(
					city=city_obj,
					pincode=row[pincode].strip(),
					address=row[address],
				)

		try:
			actormeta, amcreated = ActorMeta.objects.get_or_create(
					website=row[website] if row[website] else None,
					facebook=row[facebook] if row[facebook] else None,
					budget=int(re.sub(r"\D+",'',row[budget])) if re.sub(r"\D+",'',row[budget]) else None
				)
		except IntegrityError:
			continue

		actor = None
		try:
			actor, acreated = Actor.objects.get_or_create(
					name = row[name],
					actor_type = actor_type,
					location=location,
					tags="Wedding Photography",
					description=row[citation],
					phone = phone_number,
					alternate_phone = re.sub(r"\D+",'', row[alt_phone]) if re.sub(r"\D+",'', row[alt_phone]) else None,
					email = row[email],
					actor_meta=actormeta,
				)
		except IntegrityError:
			continue

		location.save()
		actormeta.save()
		actor.save()
		
		user = User.objects.get(username="mevin")

		# Score heuristics
		score = int(re.sub(r"\D+",'', row[user_rating])) if re.sub(r"\D+",'', row[user_rating]) else 3
		if score>5:
			score_str = str(score)
			final_score = float(score_str[0]+"."+score_str[1:])
			score = final_score

		actor.rating.add(score=score,user=user, ip_address="127.0.0.1")
		actor.save()


		google_drive_image = row[image] if 'file' in row[image] else None

		if google_drive_image:
			fileId = google_drive_image.split("/")[5].strip()

			try:
				folder_id_descriptor = DriveService.files().get(fileId=fileId).execute()
			except:
				continue
			cover_image, cover_image_name = download_file(DriveService, folder_id_descriptor)

			img_temp = NamedTemporaryFile(delete=True)
			img_temp.write(cover_image)
			img_temp.flush()

			actor.cover_image.save(
					secure_filename(cover_image_name),
					File(img_temp)
				)
			actor.save()
		print actor.name

# folder_id_descriptor = DriveService.files().get(fileId="0B2Uy2fBK4ou0M1U0ZDJseVl1WTg").execute()
# image = download_file(DriveService, folder_id_descriptor)
# output = open('image.jpg','wb')
# output.write(image)
# output.close()

