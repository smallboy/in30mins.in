# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20140912_1850'),
    ]

    operations = [
        migrations.AddField(
            model_name='actor',
            name='cover_image',
            field=models.ImageField(null=True, upload_to=b'actor_photos', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='venue',
            name='cover_image',
            field=models.ImageField(null=True, upload_to=b'venue_photos', blank=True),
            preserve_default=True,
        ),
    ]
