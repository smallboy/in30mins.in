/**
 * @jsx React.DOM
 */
 'use strict';
 var Backbone      = require('backbone'),
 ActorTypes        = require('./../collections/actor-types'),
 Select2           = require('select2');

 module.exports = Backbone.View.extend({

  el:'.actor-types-searchbox',
  cities: null,

  initialize: function(city, type){
    this.city = city;
    this.type = type;
    this.actorTypes = new ActorTypes(this.city);
    var _this = this;
    this.actorTypes.fetch({
      success: function(collection, response){
        _this.initSelectBox(response.actor_types);
      },
      error: function(){
        console.error('fetching actor types error');
      }
    });
  },

  events: {
    'change' : 'select'
  },

  initSelectBox: function(data) {

      function format(item) {
        return item.label;
      }
      this.$el.select2({
        width: 'element',
        data: {
          results: data,
          text: 'label'
        },
        formatSelection: format,
        formatResult: format,

        initSelection : function (element, callback) {
          var currentType = top.location.href.split('/', 5)[4];
          callback({ label: currentType});
        }
      });

  },

    select: function(e){
      var selectedLocation = e.added.name.toLowerCase();
      window.location = '/' + this.city +'/'+ selectedLocation;

  },

});

