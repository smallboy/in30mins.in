from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView, TemplateView
from django.contrib.gis import admin
from django.conf import settings

from core import views as CoreViews
from views import city_list_type_ahead, actor_type_type_ahead, ContactUs, About

admin.site.site_header = 'AlmostMarried.in v0.5'
admin.autodiscover()

development = patterns('',
    url(r'^$', CoreViews.LandingPageView.as_view()),

    url(r'^(?P<city>(\w+))/$', CoreViews.LandingPageInnerView.as_view()),

    url(r'^(?P<city>(\w+))/(?P<actor>\w+)/$', CoreViews.ActorListView.as_view()),
    url(r'^(?P<city>(\w+))/(?P<actor>\w+)/json/$', CoreViews.ActorListViewJson.as_view()),

    url(r'^(?P<city>(\w+))/(?P<actor>[-\w]+)/(?P<actor_id>[-\w]+)/$', CoreViews.ActorDetailView.as_view()),

)

temp = patterns('',
    url(r'^$', RedirectView.as_view(url='http://signup.almostmarried.in/')),
    url(r'^dev/', include(development)),
)

api = patterns('',
        url(r'^api/avaliable-cities/$', city_list_type_ahead),
        url(r'^api/(?P<city_name>(\w+))/actor-types/$', actor_type_type_ahead),
    )

static = patterns('',
        url(r'^contactus/$', ContactUs.as_view()),
        url(r'^about/$', About.as_view()),
    )

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    (r'^comments/', include('django_comments.urls')),
)


if settings.ON_OPENSHIFT:
    urlpatterns += api
    urlpatterns += development
else:
    urlpatterns += patterns('django.views.static',(r'media/(?P<path>.*)','serve',{'document_root': settings.MEDIA_ROOT}), )
    urlpatterns += static
    urlpatterns += api
    urlpatterns += development

urlpatterns += patterns('',
    url(r'^robots\.txt$', TemplateView.as_view(
            template_name='robots.txt',
            content_type='text/plain'
        )),
)