from setuptools import setup

import os

# Put here required packages
packages = ['Django>=1.7', 'django-enumfield', 'six', 'pillow', 'django-contrib-comments', 'google-api-python-client', 'pyopenssl', 'requests']

if 'REDISCLOUD_URL' in os.environ and 'REDISCLOUD_PORT' in os.environ and 'REDISCLOUD_PASSWORD' in os.environ:
     packages.append('django-redis-cache')
     packages.append('hiredis')

setup(name='AlmostMarried',
      version='1.5',
      description='AlmostMarried.in',
      author='Mevin Babu',
      author_email='mevinbabuc@gmail.com',
      url='https://pypi.python.org/pypi',
      install_requires=packages,
)

