from django.shortcuts import render
from django.http import Http404
from django.views.generic import View, TemplateView, ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from utils import HttpJsonResponse
from core.models import Actor, ActorEnum, ActorType, City, Location

class LandingPageView(TemplateView):
    template_name = "core/landing.html"

class LandingPageInnerView(ListView):

    template_name = "core/landing_inner.html"
    context_object_name="actor_type_list"
    is_location_set = False

    def get_queryset(self):

        is_location_set = True
        location_city = self.kwargs.get('city', None)
        if not location_city:
            location_city = "bangalore"
            is_location_set = False

        try:
            city = City.objects.get(name=location_city)
            self.is_location_set = is_location_set
        except :
            if location_city:
                raise Http404
        else:
            actor_types = Actor.objects.filter(location__city=city).values_list('actor_type', flat=True).distinct()
            return ActorType.objects.filter(id__in=actor_types)

    def get_context_data(self, **kwargs):

        context = super(LandingPageInnerView, self).get_context_data(**kwargs)

        context['is_location_set'] = self.is_location_set
        return context


class ActorListView(View):
    template_name = 'core/actor_list.html'

    def get(self, request, *args, **kwargs):

        page = request.GET.get('page')
        location_city = self.kwargs.get('city', None)

        try:
            city = City.objects.get(name=location_city)
        except :
            raise Http404()
        else:

            actor_type_name = self.kwargs['actor'].lower()
            actor_type = ActorType.objects.get(name=actor_type_name)
            actors = Actor.objects.filter(
                actor_type__name=actor_type_name,
                location__city=city).extra(
                    select={'actor_rating': '((100/%s*rating_score/(rating_votes+%s))+100)/2' % (Actor.rating.range, Actor.rating.weight)
                }).order_by('-actor_rating')

            # actor_id_list = Location.objects.filter(actor__actor_type=actor_type, city=city).values_list('actor', flat=True)
            # print len(actor_id_list)
            # actors = Actor.objects.filter(id__in=actor_id_list)
            # print len(actors)            

            paginator = Paginator(actors, 15)

            try:
                actors_in_page = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                actors_in_page = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                actors_in_page = paginator.page(paginator.num_pages)

            return render(request, self.template_name, {
                    'actors': actors_in_page,
                    'paginator': paginator.page_range,
                    'actor_type_cover_img': actor_type.cover_image,
                    'actor_type_name': actor_type.label,
                })


    def post(self, request, *args, **kwargs):

        actor_type_name = self.kwargs['actor'].lower()
        # actor_type_id = dict(ActorEnum.items())[actor_type_name]

        location_city = self.kwargs.get('city', None)
        city = City.objects.get(name=location_city)

        json_filter = dict(request.POST)
        json_filter.update({
                actor_type__name: actor_type_name,
                location__city: city.id,
            })

        actors = Actor.objects.filter(**json_filter)

        # Lets JSON Serialize this shit
        json_result_set = list()
        for actor in actors:
            actor_data_dic = dict()

            actor_data_dic['name'] = actor.name

            if actor.actor_meta:
                actor_data_dic['budget'] = actor.actor_meta.budget

            actor_data_dic['location'] = actor.location.city
            actor_data_dic['rating'] = actor.rating.score
            actor_data_dic['ratings_count'] = actor.rating.votes
            actor_data_dic['ratings_mantra'] = actor.rating.get_rating()
            actor_data_dic['thumbnail'] = actor.cover_image

            json_result_set.append(actor_data_dic)

        return HttpJsonResponse(json_result_set)



class ActorListViewJson(View):

    def get(self, request, *args, **kwargs):

        actor_type_name = self.kwargs['actor'].lower()
        # actor_type_id = dict(ActorEnum.items())[actor_type_name]
        location_city = self.kwargs.get('city', None)

        try:
            city = City.objects.get(name=location_city)
        except :
            raise Http404
        else:

            actors = Actor.objects.filter(actor_type__name=actor_type_name, location__city=city)

            # Lets JSON Serialize this shit
            json_result_set = list()
            for actor in actors:
                actor_data_dic = dict()

                actor_data_dic['name'] = actor.name

                if actor.actor_meta:
                    actor_data_dic['budget'] = actor.actor_meta.budget

                actor_data_dic['location'] = actor.location.city.name
                actor_data_dic['rating'] = actor.rating.score
                actor_data_dic['ratings_count'] = actor.rating.votes
                actor_data_dic['ratings_mantra'] = actor.rating.get_rating()
                actor_data_dic['thumbnail'] = actor.cover_image

                json_result_set.append(actor_data_dic)

            return HttpJsonResponse(json_result_set)

    def post(self, request, *args, **kwargs):

        actor_type_name = self.kwargs['actor'].lower()
        # actor_type_id = dict(ActorEnum.items())[actor_type_name]
        location_city = self.kwargs.get('city', None)

        try:
            city = City.objects.get(name=location_city)
        except :
            raise Http404
        else:

            json_filter = dict(request.POST)
            json_filter.update({
                    actor_type__name: actor_type_name,
                    location__city: city.id,
                })

            actors = Actor.objects.filter(**json_filter)

            # Lets JSON Serialize this shit
            json_result_set = list()
            for actor in actors:
                actor_data_dic = dict()

                actor_data_dic['name'] = actor.name

                if actor.actor_meta:
                    actor_data_dic['budget'] = actor.actor_meta.budget

                actor_data_dic['location'] = actor.location.city
                actor_data_dic['rating'] = actor.rating.score
                actor_data_dic['ratings_count'] = actor.rating.votes
                actor_data_dic['ratings_mantra'] = actor.rating.get_rating()
                actor_data_dic['thumbnail'] = actor.cover_image

                json_result_set.append(actor_data_dic)

            return HttpJsonResponse(json_result_set)


class ActorDetailView(View):
    template_name = 'core/actor_detail.html'

    def get(self, request, *args, **kwargs):

        actor_id = int(self.kwargs['actor_id'])
        next = request.get_full_path()

        actor = Actor.objects.get(id=actor_id)
        return render(request, self.template_name, {'actor': actor, 'next': next})
