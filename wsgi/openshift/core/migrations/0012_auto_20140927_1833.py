# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_location_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='google_map_link',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='city',
            field=models.ForeignKey(blank=True, to='core.City', null=True),
        ),
    ]
