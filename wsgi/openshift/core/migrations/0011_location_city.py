# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20140921_0710'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='city',
            field=models.ForeignKey(default=1, to='core.City'),
            preserve_default=True,
        ),
    ]
