# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_auto_20141003_0635'),
    ]

    operations = [
        migrations.AddField(
            model_name='actormeta',
            name='actor',
            field=models.OneToOneField(related_name=b'related_actor_meta', null=True, to='core.Actor'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='actor_meta',
            field=models.ForeignKey(related_name=b'meta_actor', blank=True, to='core.ActorMeta', null=True),
        ),
    ]
