'use strict';

var Backbone  = require('backbone');

module.exports = Backbone.Collection.extend({
  initialize: function(city){
    this.city = city;
  },
  url: function(){
    return '/api/'+ this.city +'/actor-types/';
  },
  defaults: {
    actor_types: null
  }
});
