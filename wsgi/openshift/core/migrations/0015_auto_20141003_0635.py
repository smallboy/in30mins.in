# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20140927_1843'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='actor',
            field=models.OneToOneField(related_name=b'related_actor', null=True, to='core.Actor'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='location',
            field=models.ForeignKey(related_name=b'location_of_actor', blank=True, to='core.Location', null=True),
        ),
    ]
