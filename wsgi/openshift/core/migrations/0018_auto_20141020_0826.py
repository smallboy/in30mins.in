# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20141020_0515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='alternate_phone',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='actor',
            name='phone',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
