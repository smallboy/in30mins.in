# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models
import django.contrib.gis.db.models.fields
import django_enumfield.db.fields
import django_enumfield.enum


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('actor_type', django_enumfield.db.fields.EnumField(default=0, enum=core.models.ActorEnum, choices=[(0, django_enumfield.enum.Value(b'UNKNOWN', 0, b'-', core.models.ActorEnum)), (1, django_enumfield.enum.Value(b'PHOTOGRAPHER', 1, b'Photographer', core.models.ActorEnum)), (2, django_enumfield.enum.Value(b'TEXTILE', 2, b'Textile', core.models.ActorEnum)), (3, django_enumfield.enum.Value(b'FLORIST', 3, b'Florist', core.models.ActorEnum)), (4, django_enumfield.enum.Value(b'EVENT_MANAGE', 4, b'Event Managers', core.models.ActorEnum))])),
                ('tags', models.CharField(max_length=255, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('phone', models.CharField(unique=True, max_length=20)),
                ('alternate_phone', models.CharField(max_length=20, unique=True, null=True, blank=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ActorImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(null=True, upload_to=b'actor_photos', blank=True)),
                ('tags', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('City', models.CharField(max_length=255)),
                ('pincode', models.CharField(max_length=8, null=True, blank=True)),
                ('address', models.TextField(null=True, blank=True)),
                ('geolocation', django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Venue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('venue_type', django_enumfield.db.fields.EnumField(default=0, enum=core.models.VenueEnum, choices=[(0, django_enumfield.enum.Value(b'UNKNOWN', 0, b'-', core.models.VenueEnum)), (1, django_enumfield.enum.Value(b'HOTEL', 1, b'Hotel', core.models.VenueEnum)), (2, django_enumfield.enum.Value(b'HALL', 2, b'Hall', core.models.VenueEnum))])),
                ('tags', models.CharField(max_length=255, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('phone', models.CharField(unique=True, max_length=20)),
                ('alternate_phone', models.CharField(max_length=20, unique=True, null=True, blank=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VenueImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(null=True, upload_to=b'venue_photos', blank=True)),
                ('tags', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VenueMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_ac', models.BooleanField(default=False)),
                ('is_food', models.BooleanField(default=False)),
                ('capacity', models.BigIntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='venue',
            name='images',
            field=models.ManyToManyField(to='core.VenueImages'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='venue',
            name='location',
            field=models.ForeignKey(blank=True, to='core.Location', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='venue',
            name='venue_meta',
            field=models.ForeignKey(blank=True, to='core.VenueMeta', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actor',
            name='images',
            field=models.ManyToManyField(to='core.ActorImages'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actor',
            name='location',
            field=models.ForeignKey(blank=True, to='core.Location', null=True),
            preserve_default=True,
        ),
    ]
