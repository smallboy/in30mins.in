require.config({
  shim: {
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    },

    backboneReact: {
      deps: [
        'backbone',
        'react'
      ],
      exports: 'BackboneReact'
    },

    growl: {
      deps: [
        'jquery'
      ],
      exports: 'Growl'
    },

    select2: {
      deps: [
        'jquery'
      ],
      exports: 'Select2'
    },

    jqueryplaceholder: {
      deps: [
        'jquery'
      ],
      exports: 'jqueryplaceholder'
    },

    foundation: {
      deps: [
        'jquery',
        'fastclick',
        'jquerycookie',
        'jqueryplaceholder',
        'modernizr'
      ],
      exports: 'Foundation'
    },

    react: {
      exports: 'React'
    },

    underscore: {
      exports: '_'
    },

  },
  baseUrl : '/static/scripts/',
  paths: {
    backbone        : '/static/libs/backbone/backbone',
    backboneReact   : '/static/libs/backbone-react-component/lib/component',
    growl           : '/static/libs/growl/javascripts/jquery.growl',
    fastclick       : '/static/libs/fastclick/lib/fastclick',
    foundation      : '/static/libs/foundation/js/foundation.min',
    jquery          : '/static/libs/jquery/dist/jquery',
    jquerycookie    : '/static/libs/jquery.cookie/jquery.cookie',
    jqueryplaceholder : '/static/libs/jquery-placeholder/jquery.placeholder',
    JSXTransformer  : '/static/libs/react/JSXTransformer',
    react           : '/static/libs/react/react-with-addons',
    select2         : '/static/libs/select2/select2.min',
    underscore      : '/static/libs/underscore/underscore',
    modernizr       : '/static/libs/modernizr/modernizr'
  }
});
