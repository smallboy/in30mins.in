'use strict';

var Backbone      = require('backbone'),
$                 = require('jquery'),
LocationSelector  = require('./views/location-selector.jsx'),
TypeSelector      = require('./views/type-selector.jsx'),
rheadroom         = require('./plugins/headroom');


module.exports = Backbone.Router.extend({
  routes: {
    ''                    : 'landing',
    ':city/'              : 'actorTypes',
    ':city/:type/'        : 'actors',
    ':city/:type/:id/'    : 'actor'

  },

  initialize: function(){
    var header = document.querySelector("#header");
    if(header) {
      this.headroom(header);
    }

  },

  headroom: function(div) {
    var headroom  = new Headroom(div);
    if(headroom){
      headroom.init();
    }
  },

  landing: function(){
    function showLandingPageDiv(){
      $('.landing-page__article').fadeTo("slow", 1);
    }
    new LocationSelector(showLandingPageDiv);
  },

  actorTypes: function(city){
    new LocationSelector();
  },

  actors: function(city, type){
    new LocationSelector();
    new TypeSelector(city, type);
  },

  actor: function(city, type, id){
    new LocationSelector();
    new TypeSelector(city, type);
  }

});

