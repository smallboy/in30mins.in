define([
  'jquery'
  ], function ($) {
    var Util = {
      windowWidth : $(window).width(),

      scroll: function(target){
        $('html,body').delay( 800 ).animate({
          scrollTop: $('#'+target).offset().top
        }, 1000);
      },

      charInDesktop: 145,
      charInMobile: 60,
      truncateString: function(string, length){
        if(!length){
          length = this.windowWidth < 600 ? this.charInMobile : this.charInDesktop;
        }
        var trimmedString = string.substr(0, length),
        LeftstringLength = string.length - length,
        tolerance = 20;

        if((string.length > length) && (LeftstringLength > tolerance)) {
          trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
          var hrefTagStarting = trimmedString.indexOf('<a');
          if(hrefTagStarting !== -1 ){
            if(trimmedString.indexOf('>', trimmedString) === -1 ) {
              trimmedString += ">";
            }
            if(trimmedString.indexOf('</a>', trimmedString) !== -1 ){
              trimmedString += "</a>";
            }
          }
          trimmedString += "...";
        }
        return trimmedString;
      },

      errorDiv: 'error',
      showError: function(message, type){
        $(this.errorDiv).addClass('alert--'+type).html(message);
      },

      Error: {
        email: 'This e-mail address has already been used for this budget participation.',
        invalidEmail : 'Invalid email! Enter a valid email.'
      },
      validate : function(type, value) {
        switch(type){
          case 'email':
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(re.test(value)){
              return true;
            }else{
              return false;
            }
          break;
        }
      },

      formError: function(error, nameDomNode){
        var $nameDomNode = $(nameDomNode),
        $parent = $nameDomNode.closest('.form-group');

        $parent.addClass('form-group--error');

        if($parent.find('.form-error').length > 0) {
          $parent.find('.form-error').text(this.Error[error]);
        }else{
          $nameDomNode.after($('<p/>',{
            'class': 'form-error',
            'text': this.Error[error],
          }));
        }
      },

      formErrorClear : function(nameDomNode){
        var $nameDomNode = $(nameDomNode),
        $parent = $nameDomNode.closest('.form-group');
        $parent.removeClass('form-group--error');
        $parent.find('.form-error').remove();
      },

      whichIE: function() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
            return(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        else                 // If another browser, return 0
            return('otherbrowser');
        return false;
      },

    };
    return Util;
  });
