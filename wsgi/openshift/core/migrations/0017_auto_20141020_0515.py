# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20141003_0810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='images',
            field=models.ManyToManyField(to=b'core.ActorImages', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='actormeta',
            name='budget',
            field=models.BigIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='actormeta',
            name='facebook',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='actormeta',
            name='website',
            field=models.URLField(null=True, blank=True),
        ),
    ]
