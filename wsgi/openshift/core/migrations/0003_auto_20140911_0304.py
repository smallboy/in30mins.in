# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20140911_0258'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='geolocation',
            field=django.contrib.gis.db.models.fields.PointField(default=b'SRID=3857;POINT(0.0 0.0)', srid=4326, dim=3),
        ),
    ]
