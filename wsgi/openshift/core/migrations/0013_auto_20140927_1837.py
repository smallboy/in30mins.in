# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20140927_1833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actormeta',
            name='budget',
            field=models.BigIntegerField(null=True),
        ),
    ]
