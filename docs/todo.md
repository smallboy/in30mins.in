# AlmostMarried.in Todo

## Front End
[x] Done Remove sign-up newletter from footer
[x] Done Rating on each tile, not a square - single digit rating bug
[x] The Actor name should be on the tile
[x] Typeahead/Autocomplete - landing page
[x] Mobile view
[] Contact Us / About us

## Back End
+ CSS/JS url fingerprinting
+ Disable BrowserSync in Prod. - Done!
+ Actor Details Page
    - Redo comments - Done!
    - Sort the details page on images,ratings - default
+ Complete Phase 2 of schema migration - Monday
+ Upload New Data
+ Admin page __unicode__ names to all the objects
+ Default images - Cover image, image gallery
+ Optimizing Image
+ Auto detect user place and make it as the default city
+ Centralized database for ads
+ User Login ?
+ Ability to add ratings

## Blog
+ Screw the juniors
+ redesing the blog
+ check out the admin area
