from django.shortcuts import render_to_response

from django.http import HttpResponse
from django.views.generic import TemplateView

from core.models import City, Actor
from utils import HttpJsonResponse


def home(request):
    return render_to_response('home/landing.html')

def city_list_type_ahead(request):
	city_list = City.objects.all().values('id', 'name')
	return HttpJsonResponse({"cities": list(city_list)})

def actor_type_type_ahead(request,city_name):
    city = City.objects.get(name=city_name)
    actor_type_list = Actor.objects.filter(location__city=city).values('actor_type__id', 'actor_type__label','actor_type__name').distinct()

    actor_result=list()
    for actor in actor_type_list:
      actor_dict = dict()
      actor_dict['id'] = actor['actor_type__id']
      actor_dict['label'] = actor['actor_type__label']
      actor_dict['name'] = actor['actor_type__name']
      actor_result.append(actor_dict)

    return HttpJsonResponse({"actor_types": list(actor_result)})

class ContactUs(TemplateView):
    template_name = "static_html/contactus.html"

class About(TemplateView):
    template_name = "static_html/about.html"