/**
 * @jsx React.DOM
 */
 'use strict';
 var Backbone      = require('backbone'),
 Cities            = require('./../collections/cities'),
 Select2           = require('select2');

 module.exports = Backbone.View.extend({
  el:'.location-searchbox',
  cities: null,

  initialize: function(callback){
    this.cities = new Cities();
    var _this = this;
    this.cities.fetch({
      success: function(collection, response){
        _this.initSelectBox(response.cities, callback);
      },
      error: function(){
        console.error('fetching cities error');
      }
    });
  },

  events: {
    'change' : 'select'
  },

  initSelectBox: function(data, parentCallback) {

    function format(item) {
      return item.name;
    }
    this.$el.select2({
      width: 'element',
      data: {
        results: data,
        text: 'name'
      },
      formatSelection: format,
      formatResult: format,

      initSelection : function (element, callback) {
        var currentLocation = top.location.href.split('/', 4)[3] || 'bangalore';
        var data = { name: currentLocation};
        callback(data);
      }
    });

    if(parentCallback){
      parentCallback();
    }
  },

  select: function(e){
    var selectedLocation = e.added.name.toLowerCase();
    var href = top.location.href.split('/',5);
    href[3] = selectedLocation;
    window.location = href.join('/');

  },

});
