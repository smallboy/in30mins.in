var Backbone  = require('backbone');
'use strict';
module.exports = Backbone.Collection.extend({
  url: '/api/avaliable-cities',
  defaults : {
    cities: []
  },
});
