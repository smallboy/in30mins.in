var ENTER_KEY = 13,
ESC_KEY = 27;

var $ = require('jquery'),
Backbone = require('backbone'),
// foundation = require('foundation'),
Router = require('./router');


var router = new Router();

if (!Backbone.History.started) {
  Backbone.history.start({pushState: true, hashChange: false});
}

  // $(document).foundation({
  //   reveal : {
  //     animation_speed: 500
  //   },
  //   topbar : {
  //   }
  // });
