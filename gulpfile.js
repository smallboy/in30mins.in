var gulp      = require('gulp'),
$             = require('gulp-load-plugins')(),
rimraf        = require('rimraf'),
runSequence   = require('run-sequence'),
browserSync   = require('browser-sync'),
pagespeed     = require('psi'),
reload        = browserSync.reload;

var path = {
  html: {
    src:'wsgi/openshift/templates/**/*.html',
  },
  scripts : {
    src: 'wsgi/assets/scripts/**/*.{js,jsx}',
    app: 'wsgi/assets/scripts/app.js',
    dist: 'wsgi/assets/dist/js/'
  },
  styles : {
    scss: 'wsgi/assets/styles/**/*.scss',
    src: 'wsgi/assets/styles/main.scss',
    css: 'wsgi/assets/dist/styles/*.css',
    dist: 'wsgi/assets/dist/styles/'
  },
  images : {
    src: 'wsgi/assets/images/**/*',
    dist: 'wsgi/assets/dist/images/'
  },
  dist: 'wsgi/assets/dist'
};

// gulp.task('scripts:copy', function () {
//   return gulp.src(path.scripts.src)
//   .pipe(gulp.dest(path.scripts.dist))
//   .pipe(reload({stream: true}));
// });
// Lint JavaScript
gulp.task('scripts:jshint', function () {
  return gulp.src(path.scripts.src)
  .pipe($.plumber({errorHandler: $.notify.onError('<%= error.message %>')}))
  // .pipe($.jshint())
  // .pipe($.jshint.reporter('jshint-stylish'))
  // .pipe($.jshint.reporter('fail'))
  .pipe($.plumber.stop())
  .pipe(reload({stream: true}));
});

gulp.task('scripts:browserify', function () {
  return gulp.src(path.scripts.app)
  .pipe($.plumber({errorHandler: $.notify.onError('<%= error.message %>')}))
  .pipe($.browserify({
    insertGlobals : true,
    transform: ['reactify','debowerify'],
    debug : true,
    shim: {
      jquery: {
        path: 'wsgi/assets/libs/jquery/dist/jquery',
        exports: '$'
      },
      select2: {
        path: 'wsgi/assets/libs/select2/select2.min',
        exports: 'Select2',
        depends: {
          jquery: 'jQuery'
        }
      },
      backbone: {
        path: 'wsgi/assets/libs/backbone/backbone',
        exports: 'Backbone',
        depends: {
          jquery: 'jQuery',
          underscore: '_',
        }
      }
    }
    // debug : !gulp.env.production
  }))
  .pipe(gulp.dest(path.scripts.dist))
  .pipe($.plumber.stop())
  .pipe(reload({stream: true}))
  .pipe($.size({title: 'script'}));
});

// Optimize Images
gulp.task('images', function () {
  return gulp.src(path.images.src)
  .pipe($.cache($.imagemin({
    progressive: true,
    interlaced: true
  })))
  .pipe(gulp.dest(path.images.dist))
  .pipe(reload({stream: true, once: true}))
  .pipe($.size({title: 'images'}));
});

// Automatically Prefix CSS
gulp.task('styles:css', function () {
  return gulp.src(path.styles.css)
.pipe($.autoprefixer('last 1 version'))
  .pipe(gulp.dest(path.styles.dist))
  .pipe(reload({stream: true}))
  .pipe($.size({title: 'styles:css'}));
});

gulp.task('styles:scss', function () {
  return gulp.src(path.styles.src)
  .pipe($.plumber({errorHandler: $.notify.onError('<%= error.message %>')}))
  .pipe($.rubySass({
    style: 'expanded',
    precision: 10,
    sourcemap: true,
    lineNumber: true,
    bundleExec: true
  }))
  .pipe($.notify('styles ready'))
  .pipe($.autoprefixer('last 2 version',
   'safari 5',
   'ie 8',
   'ie 9',
   'opera 12.1',
   'ios 6',
   'android 4'))
  .pipe($.plumber.stop())
  .pipe(gulp.dest(path.styles.dist))
  .pipe($.size({title: 'styles:scss'}));
});

// Output Final CSS Styles
gulp.task('styles', ['styles:scss', 'styles:css']);
gulp.task('scripts',['scripts:browserify']);

// Scan Your HTML For Assets & Optimize Them
gulp.task('html', function () {
  return gulp.src('*.html')
  .pipe($.useref.assets({searchPath: '{.tmp,app}'}))
    // Concatenate And Minify JavaScript
    // .pipe($.if('*.js', $.uglify()))
    // Concatenate And Minify Styles
    .pipe($.if('*.css', $.csso()))
    // Remove Any Unused CSS
    // Note: If not using the Style Guide, you can delete it from
    // the next line to only include styles your project uses.
    // .pipe($.if('*.css', $.uncss({ html: ['app/index.html'] }))) //,'app/styleguide/index.html'
    .pipe($.useref.restore())
    .pipe($.useref())
    // Update Production Style Guide Paths
    // .pipe($.replace('styles/main.css', 'styles/main.min.css'))
    // Minify Any HTML
    // .pipe($.minifyHtml())
    // Output Files
    .pipe(gulp.dest(path.dist))
    .pipe($.size({title: 'html'}));
});

// Clean Output Directory
gulp.task('clean', function (cb) {
  rimraf('dist', rimraf.bind({}, '.tmp', cb));
});

// Watch Files For Changes & Reload
gulp.task('watch', function () {
  // browserSync.use(require("bs-snippet-injector"), {
  //     file: "wsgi/openshift/templates/base.html"
  // });
  browserSync({
    // proxy: "http://127.0.0.1:8000",
    notify: false,
    ghostMode: {
      clicks: false,
      location: true,
      forms: true,
      scroll: true
    }
  });

  gulp.watch([path.html.src],     reload);
  gulp.watch([path.styles.scss],  ['styles:scss']);
  gulp.watch([path.styles.css],   ['styles:css']);
  gulp.watch([path.scripts.src],  ['scripts:browserify']);
  gulp.watch([path.images.src],   ['images']);
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], function (cb) {
  runSequence('styles', ['scripts', 'html', 'images'], cb);
});

// Run PageSpeed Insights
// Update `url` below to the public URL for your site
gulp.task('pagespeed', pagespeed.bind(null, {
  // By default, we use the PageSpeed Insights
  // free (no API key) tier. You can use a Google
  // Developer API key if you have one. See
  // http://goo.gl/RkN0vE for info key: 'YOUR_API_KEY'
  url: 'almostmarried.in',
  strategy: 'mobile'
}));
