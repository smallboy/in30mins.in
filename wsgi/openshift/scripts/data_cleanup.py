from core.models import Actor, City, Location
"""
Handle with care
"""

def merge_citis(actual_city_id, duplicate_city_id_list):
	"""
	Merge cities with wrong names together
	"""

	city = City.objects.get(id=actual_city_id)

	city_14_15_list = Location.objects.filter(city__in=duplicate_city_id_list)

	for location in city_14_15_list:
		location.city = city
		location.save()

	return True


def location_actor_relation_migration():
	"""
	Migrate relation from Actor->Location to Location->Actor
	"""

	actor_list = Actor.objects.all()
	for each in actor_list:
		each.location.actor = each
		each.location.save()
		each.save()

	return True


def actor_meta_actor_relation_migration():
	"""
	Migrate relation from Actor->ActorMeta to ActorMeta->Actor
	"""

	actor_list = Actor.objects.all()
	for actor in actor_list:
		actor.actor_meta.actor = actor
		actor.actor_meta.save()
		actor.save()

	return True


def upload_data():

	import os
	import re
	import csv


	from django.core.files import File
	from django.contrib.auth.models import User
	from django.db import IntegrityError, DataError
	from django.core.files.temp import NamedTemporaryFile

	_split = re.compile(r'[\0%s]' % re.escape(''.join(
		[os.path.sep, os.path.altsep or ''])))

	def secure_filename(path):
		return _split.sub('', path)

	actor_type_list = [
		("photographer",3),
		("wear", 4),
		("jewellers", 5),
		("florists", 6),
		("planners", 7),
		("makeup", 8),
		("caterers", 9),
		("bakers", 10),
		("astrology", 14)
	]

	def load_data(actor_type_extract, actor_type_extract_id):
		with open('data/'+actor_type_extract+'.csv','rb') as csvfile:
			reader = csv.reader(csvfile, delimiter='|')

			slno = 0
			name = 1
			address = 2
			city = 3
			pincode = 4
			google_map = 5
			website = 6
			email = 7
			phone = 8
			alt_phone = 9
			budget = 10
			citation = 11
			facebook = 12
			user_rating = 13

			from core.models import Actor, ActorType, ActorMeta, Location, City

			for row in reader:


				try:
					phone_number = re.sub(r"\D+",'', row[phone]) if re.sub(r"\D+",'', row[phone]) else None
				except:
					print "Phone Number error"
					continue
				if not phone_number:
					print "No Phone number"
					continue

				try:
					actor_type = ActorType.objects.get(id=actor_type_extract_id)
				except:
					print "No actor type"
					continue

				city_name = row[city].strip().lower() if row[city].strip() else None
				city_obj = None
				if city_name:
					city_obj, ccreated = City.objects.get_or_create(name=city_name)
				else:
					print "No City "
					continue

				actor = None
				try:
					actor, acreated = Actor.objects.get_or_create(
							name = row[name],
							actor_type = actor_type,
							tags="Wedding "+actor_type_extract,
							description=row[citation],
							phone = phone_number,
							alternate_phone = re.sub(r"\D+",'', row[alt_phone]) if re.sub(r"\D+",'', row[alt_phone]) else None,
							email = row[email],
						)
					actor.save()
				except IntegrityError:
					print "Actor creation integrity error"
					print row[name], row[citation], phone_number, re.sub(r"\D+",'', row[alt_phone]) if re.sub(r"\D+",'', row[alt_phone]) else None, row[email]
					ac = Actor.objects.get(phone=phone_number)
					print ac.name, ac.description, ac.phone, ac.alternate_phone, ac.email
					print ""
					continue
				except DataError:
					print row[name], actor_type_extract
					continue

				try:
					location, lcreated = Location.objects.get_or_create(
							city=city_obj,
							pincode=row[pincode].strip(),
							address=row[address],
							google_map_link = row[google_map],
							actor = actor,
						)
					location.save()
					actor.location = location
					actor.save()
				except DataError:
					location, lcreated = Location.objects.get_or_create(
							city=city_obj,
							pincode=row[pincode].strip(),
							address=row[address],
							actor = actor,
						)
					location.save()
					actor.location = location
					actor.save()
				except IntegrityError:
					print "Location ",actor.name, actor.id		
				try:
					actormeta, amcreated = ActorMeta.objects.get_or_create(
							website=row[website] if row[website] else None,
							facebook=row[facebook] if row[facebook] else None,
							budget=int(re.sub(r"\D+",'',row[budget])) if re.sub(r"\D+",'',row[budget]) else None,
							actor=actor,
						)
					actormeta.save()
					actor.actor_meta = actormeta
					actor.save()
				except IntegrityError:
					print "Actor Meta IntegrityError"
					continue
				except DataError:
					print row[name], actor_type_extract
					continue

				user = User.objects.get(username="mevin")

				# Score heuristics
				try:
					score = int(re.sub(r"\D+",'', row[user_rating])) if re.sub(r"\D+",'', row[user_rating]) else 3
				except:
					score = 3
				if score>5:
					score_str = str(score)
					final_score = float(score_str[0]+"."+score_str[1:])
					score = final_score

				actor.rating.add(score=score,user=user, ip_address="127.0.0.1")
				actor.save()

				if row[slno]:
					cover_image = None
					is_jpg = False
					is_png = False
					try:
						filename = row[slno]+".jpg"
						cover_image = open("data/"+actor_type_extract+"/"+filename)
						is_jpg = True
					except IOError:
						try:
							filename = row[slno]+".png"
							cover_image = open("data/"+actor_type_extract+"/"+filename)
							is_png = True
						except IOError:
							print "File IO error"
							pass
						pass

					if cover_image:
						actor_slug = actor.name.replace(" ",'_')
						new_filename = row[slno]
						if is_jpg:
							new_filename = actor_slug+".jpg"
						elif is_png:
							new_filename = actor_slug+".png"

						actor.cover_image.save(
								secure_filename(new_filename),
								File(cover_image)
							)
						actor.save()


						print new_filename
					else:
						print "PANIC--____-----+==", actor.id, row[slno]

	for each in actor_type_list:
		try:
			load_data(each[0], each[1])
			print "done"
		except IOError:
			print "No data "+each[0]
