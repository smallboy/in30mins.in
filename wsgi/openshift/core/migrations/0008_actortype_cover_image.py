# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20140921_0331'),
    ]

    operations = [
        migrations.AddField(
            model_name='actortype',
            name='cover_image',
            field=models.ImageField(null=True, upload_to=b'actor_type_photos', blank=True),
            preserve_default=True,
        ),
    ]
