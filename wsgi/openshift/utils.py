import json
import datetime

from django.http import HttpResponse


def serialize_to_json(data):
    result = None
    if isinstance(data, datetime.datetime) or isinstance(data, datetime.date):
        result = data.isoformat()
    return result


def HttpJsonResponse(context, **kwargs):
    data = json.dumps(context, default=serialize_to_json)
    kwargs['content_type'] = 'application/json; charset=utf-8'
    return HttpResponse(data, **kwargs)

