# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_actortype_cover_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='actortype',
            old_name='labels',
            new_name='label',
        ),
        migrations.AlterField(
            model_name='location',
            name='geolocation',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3, null=True, blank=True),
        ),
    ]
