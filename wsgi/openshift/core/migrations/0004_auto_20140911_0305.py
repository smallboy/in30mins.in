# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20140911_0304'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='geolocation',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3),
        ),
    ]
