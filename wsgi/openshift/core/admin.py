from django.contrib import admin
from django.contrib.gis import admin

from core.models import City, Location, ActorImages, Actor, ActorType, ActorMeta


admin.site.register(ActorImages)
admin.site.register(ActorType)
admin.site.register(ActorMeta)

class LocationInline(admin.StackedInline):
    fk_name = 'actor'
    model = Location
    exclude = ('geolocation', 'google_map_link')

class ActorMetaInline(admin.StackedInline):
    fk_name = 'actor'
    model = ActorMeta


class ActorAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = ('name', 'actor_type', 'location', 'phone', 'created_at')
    search_fields = ('name', 'phone')
    list_filter = ('actor_type', 'location__city')
    inlines = [
        LocationInline,
        ActorMetaInline,
    ]
admin.site.register(Actor, ActorAdmin)


# Location specific admin
admin.site.register(City)

class LocationAdmin(admin.GeoModelAdmin):
    list_display = ('city', 'pincode', 'address')
    search_fields = ('address', 'city')
    list_filter = ('city', 'pincode')
admin.site.register(Location, LocationAdmin)


# from core.models import VenueMeta, VenueImages, Venue
# admin.site.register(VenueMeta)
# admin.site.register(VenueImages)
# admin.site.register(Venue)

