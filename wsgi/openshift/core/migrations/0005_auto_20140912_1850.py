# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20140911_0305'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActorMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('website', models.URLField(unique=True, null=True, blank=True)),
                ('facebook', models.URLField(unique=True, null=True, blank=True)),
                ('budget', models.BigIntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='actor',
            name='actor_meta',
            field=models.ForeignKey(blank=True, to='core.ActorMeta', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actor',
            name='rating_score',
            field=models.IntegerField(default=0, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actor',
            name='rating_votes',
            field=models.PositiveIntegerField(default=0, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='venue',
            name='rating_score',
            field=models.IntegerField(default=0, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='venue',
            name='rating_votes',
            field=models.PositiveIntegerField(default=0, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
