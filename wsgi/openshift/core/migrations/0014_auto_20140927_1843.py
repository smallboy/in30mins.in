# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20140927_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='phone',
            field=models.CharField(max_length=20, unique=True, null=True, blank=True),
        ),
    ]
